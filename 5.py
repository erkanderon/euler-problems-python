import fractions


def find_smallest():
	result = 1
	for i in range(1, 21):
		result *= i // fractions.gcd(i, result)

	return result


print(find_smallest())
