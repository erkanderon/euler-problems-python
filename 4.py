


def find_palindrome():
	found = False
	palindromes = []
	while found == False:

		for i in range(900,1000):

			for j in range(900,1000):
				param = i*j
				if(is_palindrome(param)):
					palindromes.append(param)

		found = True
	
	return biggest_palindrome(palindromes)

def biggest_palindrome(palindrome):

	return palindrome[len(palindrome)-1]

def is_palindrome(number):

	number = str(number)

	first = 0
	last = len(number)-1

	while first != last and first-last != 1:
		if number[first]!=number[last]:
			return False
		first = first + 1
		last = last - 1
	return True			
				

#print(is_palindrome(1000201))
#print(is_palindrome(91929283))
print(find_palindrome())	
