
def get_sum_fibonacci(number):

	first = 1
	second = 2
	total = 3

	while True:

		next_element = first + second
		if next_element>number:
			break
		total += next_element
		first = second
		second = next_element

	return total


print(get_sum_fibonacci(4000000))
