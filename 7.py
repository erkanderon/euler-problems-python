

def check_is_prime(number):

	for i in range(2, number):

		if number%i == 0:
			return False

	return True


def find_10001_prime():

	counter = 0
	looper = 2
	while counter != 10001:

		if check_is_prime(looper):
			counter = counter + 1
		looper = looper + 1
	return looper-1

print(find_10001_prime())
