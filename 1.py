import sys


def get_sum(total, number):
	
	if number == 0:
		return total

	if number%3==0 or number%5==0:

		total += number

	return get_sum(total, number-1)

## Test
result = get_sum(0, 9)
print(result)
